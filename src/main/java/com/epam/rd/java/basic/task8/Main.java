package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entities.Cars;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		try {


			// get container
			DOMController domController = new DOMController(xmlFileName);
			// PLACE YOUR CODE HERE
			domController.parse(true);
			Cars cars = domController.getCars();
			// sort (case 1)
			// PLACE YOUR CODE HERE
			Sorter.sortCarsByName(cars);
			// save
			String outputXmlFile = "output.dom.xml";
			// PLACE YOUR CODE HERE
			DOMController.saveToXML(cars,outputXmlFile);
			////////////////////////////////////////////////////////
			// SAX
			////////////////////////////////////////////////////////

			// get
			SAXController saxController = new SAXController(xmlFileName);
			// PLACE YOUR CODE HERE
			saxController.parse(true);
			cars = saxController.getCars();
			// sort  (case 2)
			// PLACE YOUR CODE HERE
			Sorter.sortCarsByPrice(cars);
			// save
			outputXmlFile = "output.sax.xml";
			// PLACE YOUR CODE HERE
			DOMController.saveToXML(cars,outputXmlFile);
			////////////////////////////////////////////////////////
			// StAX
			////////////////////////////////////////////////////////

			// get
			STAXController staxController = new STAXController(xmlFileName);
			// PLACE YOUR CODE HERE
			staxController.parse();
			cars = staxController.getCars();
			// sort  (case 3)
			// PLACE YOUR CODE HERE
			Sorter.sortCarsByEnginePower(cars);
			// save
			outputXmlFile = "output.stax.xml";
			// PLACE YOUR CODE HERE
			DOMController.saveToXML(cars,outputXmlFile);
		} catch (ParserConfigurationException e) {
		System.err.println(e.getMessage());
	} catch (SAXException e) {
		System.err.println(e.getMessage());
	} catch (IOException e) {
		System.err.println(e.getMessage());
	} catch (TransformerException e) {
		System.err.println(e.getMessage());
	} catch (XMLStreamException e) {
		System.err.println(e.getMessage());
	}
	}

}
