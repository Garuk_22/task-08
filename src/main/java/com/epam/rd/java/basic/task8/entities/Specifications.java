package com.epam.rd.java.basic.task8.entities;

public class Specifications {

    private int price;

    private Engine engine;

    private Dimensions dimensions;

    private FuelConsumption fuelConsumptions;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }

    public FuelConsumption getFuelConsumptions() {
        return fuelConsumptions;
    }

    public void setFuelConsumptions(FuelConsumption fuelConsumptions) {
        this.fuelConsumptions = fuelConsumptions;
    }

    @Override
    public String toString() {
        return
                "\n\t\tprice=" + price +
                "\n\t\tengine=" + engine +
                "\n\t\tdimensions=" + dimensions +
                "\n\t\tfuelConsumptions=" + fuelConsumptions +"\n";
    }
}
